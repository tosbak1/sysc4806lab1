class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :commenttext
      t.string :author
      t.string :blogpost_id
      t.string :integer

      t.timestamps null: false
    end
  end
end
